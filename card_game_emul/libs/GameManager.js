// This file handles all socket.io connections and manages the serverside game logic.
const io = require('socket.io')();
const http = require("http");
const axios = require('axios');
const stompit = require('stompit');

module.exports = class GameManager {
    constructor() {
        this.cards = []; 
        this.options = {
            hostname: 'localhost',
            port: 8082,
            path: '/cards',
            method: 'GET',
        };
        this.idRoom=0;
        this.bet =0;
        this.idCurrentPlayer = 0;
        this.pointCurrentPlayer = 0;
        this.cardsOnField = { "idPlayerOne":0,"idPlayerTwo":0,"playerOne":[],"playerTwo":[]};
        this.isTurnOne = false;
        this.endMatch = false;
        this.firstTurn = true;
        this.messagesSocket = [];
        this.playerOneDeck = [];
        this.playerTwoDeck = [];
    }

    takeAllCards() 
    {
        let self = this;
        var req = http.get(this.options, function(res) {
            res.setEncoding('utf8');
            let data = "";
            res.on('data', (chunk) => {
                data += chunk;
            });
            res.on('end', () => {
                for(let card of  JSON.parse(data)) 
                {
                    self.cards.push(card);
                }
            });
            
        });
    }

    startAMatch(id,idRoom,bet) 
    {
        this.idRoom = idRoom; //TODO : Generate
        this.cardsOnField.idPlayerOne = id;
        this.bet = bet
        this.takeAllCards()
    }

    beginAMatch(id) 
    {
        if(id != this.cardsOnField.idPlayerOne)
        {
            let deckPlayer = this.chooseDeck(this.cardsOnField.idPlayerOne);
            this.storeASocket('choose your deck for the game', {idRoom:this.idRoom,idPlayer:this.cardsOnField.idPlayerOne,deck:deckPlayer});
            deckPlayer = this.chooseDeck(id);
            this.storeASocket('choose your deck for the game', {idRoom:this.idRoom,idPlayer:id,deck:deckPlayer});
            this.cardsOnField.idPlayerTwo = id; 
        }  
        else 
        {
            this.stateOfMatch();
        }    
    }

    finishYourDeck(deckPlayer,idPlayer)
    {
        console.log("Your deck is choose")
        console.log(deckPlayer)
        if(idPlayer == this.cardsOnField.idPlayerOne)
        {
            //TODO : Add test on cards 
            this.playerOneDeck = deckPlayer;     
        }
        if(idPlayer == this.cardsOnField.idPlayerTwo)
        {
            this.playerTwoDeck = deckPlayer;
        }
        
        console.log(this.playerOneDeck.length)
        if(this.playerTwoDeck.length != 0 && this.playerOneDeck.length != 0)
        {
            this.idCurrentPlayer = this.cardsOnField.idPlayerOne;
            this.isTurnOne = true;
            this.beginATurn();
        }
        return true;
    }

    beginATurn()
    {  
        this.pointCurrentPlayer = 10;
        let data = {idRoom:this.idRoom,idPlayer:this.idCurrentPlayer,actionPoints:this.pointCurrentPlayer};
        this.storeASocket('new turn', data);
    }

    sendADeck(idPlayer)
    {
        if(idPlayer == this.cardsOnField.idPlayerOne)
        {
            return this.playerOneDeck;
        }
        
        return this.playerTwoDeck;
    }

    aCardIsPlay(idPlayer,idCard)
    {
        let cost = 3;
        let card = this.cards.filter(card => card.id == idCard)[0];
        let ret = false;

        if(card != undefined) {

            if(card.userId == idPlayer && this.idCurrentPlayer == idPlayer && this.pointCurrentPlayer > cost)
            {
                if(this.isTurnOne)
                {
                    console.log(this.cardsOnField.playerOne.includes(card))
                    if (this.cardsOnField.playerOne.length < 5 && !(this.cardsOnField.playerOne.includes(card)))
                    {
                        this.playerOneDeck.splice(this.playerOneDeck.indexOf(card), 1);
                        this.cardsOnField.playerOne.push(card);
                        ret = true;
                        this.storeASocket('your deck for the game', {idRoom:this.idRoom,idPlayer:this.idCurrentPlayer,deck:this.playerOneDeck});
                    }    
                }
                else
                {
                    if (this.cardsOnField.playerTwo.length < 5 && !(this.cardsOnField.playerTwo.includes(card)))
                    {
                        this.playerTwoDeck.splice(this.playerTwoDeck.indexOf(card), 1);
                        this.cardsOnField.playerTwo.push(card);
                        ret = true;
                        this.storeASocket('your deck for the game', {idRoom:this.idRoom,idPlayer:this.idCurrentPlayer,deck:this.playerTwoDeck});
                    }
                } 
                if (ret)
                {
                    
                    this.pointCurrentPlayer -= cost;
                    this.storeASocket('update cards', {idRoom:this.idRoom,"idPlayer":this.idCurrentPlayer,"actionPoints":this.pointCurrentPlayer,"cards":this.cardsOnField});
                }
            
        }
    }
        return ret;
    }
    
    chooseDeck(idPlayer) {
        let deck = [];
        for(let element of this.cards)
        {
            if(element.userId == idPlayer)
            {
                deck.push(element)
            }
        }
        console.log(deck)
        return deck;
    }

    anAttackIsPlay(idPlayer,idcardStricker,idcardDefenser)
    {
        let cost = 2;
        var strickerCard;
        var defenserCard; 
        let ret = false;
        if(this.pointCurrentPlayer > cost && idPlayer == this.idCurrentPlayer)
        {
            if(this.isTurnOne)
            {
                strickerCard = this.cardsOnField.playerOne.filter(card => card.id == idcardStricker)[0];
                defenserCard = this.cardsOnField.playerTwo.filter(card => card.id == idcardDefenser)[0];
            }
            else 
            {
                strickerCard = this.cardsOnField.playerTwo.filter(card => card.id == idcardStricker)[0];
                defenserCard = this.cardsOnField.playerOne.filter(card => card.id == idcardDefenser)[0];
            }

            if(strickerCard != undefined && defenserCard  != undefined)
            {
                defenserCard.hp -= strickerCard.attack;
                console.log(strickerCard.attack)
                console.log(defenserCard.hp)
                if(defenserCard.hp <= 0 && this.isTurnOne)
                {
                    this.cardsOnField.playerTwo.splice(this.cardsOnField.playerTwo.indexOf(defenserCard), 1);
                }
                else if (defenserCard.hp > 0 && this.isTurnOne) {
                    this.cardsOnField.playerTwo.filter(card => card.id == idcardDefenser).hp = defenserCard.hp
                }
                if(defenserCard.hp <= 0 && !this.isTurnOne)
                {
                    this.cardsOnField.playerOne.splice(this.cardsOnField.playerOne.indexOf(defenserCard), 1);
                }
                else if (defenserCard.hp > 0 && !this.isTurnOne) {
                    this.cardsOnField.playerOne.filter(card => card.id == idcardDefenser).hp = defenserCard.hp
                }
                this.pointCurrentPlayer -= cost;
                ret = true;
                this.storeASocket('update cards', {idRoom:this.idRoom,"idPlayer":this.idCurrentPlayer,"actionPoints":this.pointCurrentPlayer,"cards":this.cardsOnField});
            }
        }
        return ret;
    }

    hitAPlayer(id) 
    {
        console.log("try to hit a player");
        let cost = 2;
        if(this.idCurrentPlayer == id )
        {
            if(id == this.cardsOnField.idPlayerOne && this.cardsOnField.playerTwo.length == 0 && this.cardsOnField.playerOne.length != 0 && !this.firstTurn && this.pointCurrentPlayer >= cost)
            {
                this.endMatch = true
            }
            if(id == this.cardsOnField.idPlayerTwo && this.cardsOnField.playerTwo.length != 0&& this.cardsOnField.playerOne.length == 0 &&  !this.firstTurn && this.pointCurrentPlayer >= cost)
            {
                this.endMatch = true
            }

            if(this.endMatch)
            {
                this.storeASocket('hit a player', {idRoom:this.idRoom,message:"1 dammage"});
            }
        }
    }

    stateOfMatch() 
    {
        this.storeASocket('update cards', {idRoom:this.idRoom,"idPlayer":this.idCurrentPlayer,"actionPoints":this.pointCurrentPlayer,"cards":this.cardsOnField});
    }

    endTurn(idPlayer)
    {  
        if (this.idCurrentPlayer == idPlayer)
        {
            this.pointCurrentPlayer = 0;
            this.firstTurn = false;
            console.log("Fin du tour : ");
            console.log(this.endTurn);
            if(!this.endMatch)
            {
                if(!this.isTurnOne) {
                    this.idCurrentPlayer = this.cardsOnField.idPlayerOne;
                    this.isTurnOne = true;
                }
                else 
                {
                    this.idCurrentPlayer = this.cardsOnField.idPlayerTwo;
                    this.isTurnOne = false;
                }
                this.beginATurn();
            }
            else 
            {
                console.log("Un joueur a été touché ")
                this.storeASocket('endGame', {idRoom:this.idRoom,winner:this.idCurrentPlayer}); 
                let resultPlayerOne; 
                let resultPlayerTwo;
                if(this.idCurrentPlayer == this.cardsOnField.idPlayerOne)
                {
                    resultPlayerOne = ""+this.bet
                    resultPlayerTwo = "-"+this.bet
                }
                else 
                {
                    resultPlayerOne = "-"+this.bet
                    resultPlayerTwo = ""+this.bet
                    
                }
                
                /*const dataOne = {
                    id: this.cardsOnField.idPlayerOne,
                    bet: resultPlayerOne
                };
                const dataTwo = {
                    id: this.cardsOnField.idPlayerTwo,
                    bet: resultPlayerTwo
                };
                  */  
                //let retA = axios.put('http://localhost:8082/users/account?id='+this.cardsOnField.idPlayerOne+"&bet="+resultPlayerOne, dataOne);
                //let retB = axios.put('http://localhost:8082/users/account?id='+this.cardsOnField.idPlayerTwo+"&bet="+resultPlayerTwo, dataTwo);
                
                const connectOptions = {
                    'host': 'localhost',
                    'port': 61613
                };
                const headers = {
                    'destination': '/topic/RESULT_BUS_DUEL'
                };

                stompit.connect(connectOptions, (error, client) => {
                    if (error) {
                        return console.error(error);
                    }
                    const frame = client.send(headers);
                    frame.write(JSON.stringify({idPlayerOne:this.cardsOnField.idPlayerOne.toString(),betPlayerOne: resultPlayerOne,idPlayerTwo:this.cardsOnField.idPlayerTwo.toString(),betPlayerTwo: resultPlayerTwo}));
                    frame.end();
                    client.disconnect();
                });
            }   
        }
        
    }   


    storeASocket(header,msg) 
    {
        this.messagesSocket.push({"header":header,"msg":msg});  
        console.log(this.messagesSocket)     
    }

    howMuchSockets() 
    {
        return this.messagesSocket.length;       
    }

    firstSocketToSend()
    { 
        return this.messagesSocket.shift();
    }
}