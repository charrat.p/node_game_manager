// Source : https://github.com/tobloef/card-fu

var socket = io();
let playerActionPoint = 0;
let yourTurn = false;
const attackCost = 0;

//////////  Socket Events  \\\\\\\\\\
socket.on("enter match", function() 
{
	enterMatch();
});

socket.on("your turn", function(points) 
{
	yourTurnToPlay(points);
});

socket.on("update cards", function(cards) 
{
	updateCards(cards);
});

socket.on("give action points", function(result) 
{
	changeActionPoints(result);
});

socket.on("end match", function(winner, reason) 
{
	matchWinner = winner;
	matchEndReason = reason;
	readyToEnd = true;
	if (canPlayCard) {
		endMatch();
	}
});


// Functions 

function enterMatch() 
{
	console.log("La partie a commencé");
}

function yourTurnToPlay(points) 
{
    playerActionPoint = points;
    yourTurn = true;
}

function playCard(carte) 
{
    if(yourTurn && carte.cost < playerActionPoint)
    {
        //TODO POST CARTE -> game
        playerActionPoint = playerActionPoint - carte.cost; 
    }
}

function playCard(carte) 
{
    if(yourTurn && attackCost < playerActionPoint)
    {
        //TODO POST idCarteU icCarteAd -> game
        playerActionPoint = playerActionPoint - attackCost;
    }
}

function endTurn() 
{
    //TODO POST true /endTurn 
    playerActionPoint = 0;
    yourTurn = false;
}

function updateCards(cards) 
{
    for(var carte of cards) 
    {
        // Draw card on battelfield 
    }
}